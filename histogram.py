import os
import ROOT

from AtlasStyle import AtlasStyle
AtlasStyle.SetAtlasStyle()

class HistoDct(dict):

    def add_hist_1D(self, name, x_label, nbins, xmin, xmax, options=[]):
        hist = ROOT.TH1D(name, '', nbins, xmin, xmax)
        hist.SetXTitle(x_label)
        hist.SetYTitle('Entries')
        for opt in options:
            hist.SetOption(opt)
        self[name] = hist

    def add_hist_1D_int(self, name, x_label, xmin, xmax, options=[]):
        hist = ROOT.TH1I(name, '', xmax-xmin, xmin, xmax)
        hist.SetXTitle(x_label)
        hist.SetYTitle('Entries')
        for opt in options:
            hist.SetOption(opt)
        self[name] = hist

    def add_hist_2D(self, name, x_label, y_label,
                    nbins_x, xmin, xmax, nbins_y, ymin, ymax,
                    options=[]):
        hist = ROOT.TH2D(name, '', nbins_x, xmin, xmax,
                                nbins_y, ymin, ymax)
        hist.SetXTitle(x_label)
        hist.SetYTitle(y_label)
        for opt in options:
            hist.SetOption(opt)
        self[name] = hist

# Helper functions
def ensure_directory_exists(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != os.errno.EEXIST:
            raise e

def save_histograms(hists, out_dir):
    c1 = ROOT.TCanvas('c1', 'c1', 0, 0, 800, 600)
    ensure_directory_exists(out_dir)
    for hist in hists:
        filename = os.path.join(out_dir, hist+'.png')
        hists[hist].Draw()
        c1.Print(filename)
